package Challenge2;

import java.util.List;

public interface MathAction {
    public Integer modeFinder(List<Integer> listValue);
    public double meanFinder(List<Integer> listValue);
    public double medianFinder(List<Integer> listValue);
}

