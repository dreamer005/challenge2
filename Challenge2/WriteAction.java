package Challenge2;


public interface WriteAction {
    public void writeFreqFinder(String path, String saveFileFreq);
    public void writeMeanMedMod(String path, String saveMeanMedMod);
}
