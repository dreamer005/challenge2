package Challenge2;



import java.io.*;
import java.util.*;

public class ReadWriteFile implements WriteAction, ReadAction, MathAction, DeleteAction{

    /*-----------------------READ DATA-------------------------*/
    @Override
    public List<Integer> readFile(String path) {
        try {
            File file = new File(path);
            FileReader fr = new FileReader(file);
            BufferedReader br = new BufferedReader(fr);

            String line = " ";
            String[] tempArr;

            List<Integer> listValues = new ArrayList<>();
            while ((line = br.readLine()) != null) {
                tempArr = line.split(";");

                for (int i = 0; i < tempArr.length; i++) {
                    if (i > 0) {
                        String temp = tempArr[i];
                        Integer intTemp = Integer.parseInt(temp);
                        listValues.add(intTemp);
                    }
                }
            }
            Collections.sort(listValues);
            br.close();
            return listValues;

        } catch (IOException ioe) {
            System.out.println("\n\n\n\n\n\n\n\n\nThere is an error path: " + ioe.getMessage());
        }
        return null;
    }

    /*-------------------------WRITE FREQUENCY-------------------------*/
    @Override
    public void writeFreqFinder(String path, String saveFileFreq) {

        try {
            File file = new File(saveFileFreq);
            if(file.createNewFile()) {
                System.out.println("File tersimpan di: " + saveFileFreq);
            }
            FileWriter writer = new FileWriter(file);
            BufferedWriter bwr = new BufferedWriter(writer);

            List<Integer> tempValues = new ArrayList<>(readFile(path));

            Set<Integer> diffValue = new HashSet<>(tempValues);

            Map<Integer,Integer> distribValue = new HashMap<>();

            bwr.write("Berikut Hasil Pengolahan Nilai:\n");
            bwr.write("\n");
            bwr.write("Nilai\t\t" + "|\t\t" + "Frekuensi\n");
            bwr.write("\n");

            for ( Integer a : diffValue) {
                Integer freq = 0;
                for ( Integer b : tempValues){
                    if (a.equals(b)){
                        freq ++;
                    } else if ( b > a){
                        break;
                    }
                }
                distribValue.put(freq, a);
                bwr.write(distribValue.get(freq) + "\t\t\t" + "|\t\t" + freq + "\n");
            }
            bwr.flush();
            bwr.close();
        } catch(IOException ioe) {
            System.out.println("\n\n\n\n\n\n\n\n\nThere is an error path: " + ioe.getMessage());
        }
    }

    /*---------------------WRITE MEAN MEDIAN MOD----------------------*/
    @Override
    public void writeMeanMedMod(String path, String saveMeanMedMod) {


        try {
            File file = new File(saveMeanMedMod);
            if(path != null || file.createNewFile()) {
                System.out.println("File tersimpan di: " + saveMeanMedMod);
            }
            FileWriter writer = new FileWriter(file);
            BufferedWriter bwr = new BufferedWriter(writer);

            bwr.write("Berikut Hasil Pengolahan Nilai:\n");
            bwr.write(" \n");
            bwr.write("Berikut hasil sebaran data nilai\n");


            bwr.write("Mean   : " + String.format("%.2f", meanFinder(readFile(path))) + "\n");
            bwr.write("Median : " + String.format("%.2f", medianFinder(readFile(path))) + "\n");
            bwr.write("Modus  : " + modeFinder(readFile(path)) + "\n");

            bwr.flush();
            bwr.close();
        } catch (IOException ioe) {
            System.out.println("There is an error " + ioe.getMessage());
        }
    }

    /*-------------------------MODE-------------------------*/
    @Override
    public Integer modeFinder(List<Integer> listValue) {
        List<Integer> tempValues = new ArrayList<>(listValue);

        Set<Integer> diffValue = new HashSet<>(tempValues);

        Map<Integer,Integer> distribValue = new HashMap<>();

        for ( Integer a : diffValue) {
            Integer freq = 0;
            for ( Integer b : tempValues){
                if (a.equals(b)){
                    freq ++;
                } else if ( b > a){
                    break;
                }
            }
            distribValue.put(freq, a);
        }
        Integer maxKey = Collections.max(distribValue.keySet());
        return distribValue.get(maxKey);
    }

    /*-------------------------MEAN-------------------------*/
    @Override
    public double meanFinder(List<Integer> listValue) {
        double mean;
        double sum = 0;
        for (Integer integer : listValue) {
            sum += integer;
        }
        mean = sum / listValue.size();
        return mean;
    }

    /*-------------------------MEDIAN-------------------------*/
    @Override
    public double medianFinder(List<Integer> listValue) {
        double median;
        if (listValue.size() % 2 == 0)
            median = (double)( listValue.get(listValue.size() / 2 ) + listValue.get(listValue.size() / 2 + 1 )) / 2;
        else
            median = (double) listValue.get(listValue.size() / 2);
        return median;
    }

    @Override
    public void multiDelete() {
        File deletefl = new File("src/tempfile");

        String[] s = deletefl.list();

        for (String a : s){
            File f1 = new File(deletefl,a);
            f1.delete();
        }
        System.out.println("SHUTDOWN AND DELETING TEMP FILE SUCCESSFULL... ");
    }
}

