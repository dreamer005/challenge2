package Challenge2;



import Challenge2.ReadWriteFile;
import Challenge2.ReadWriteKelas;

import java.util.Scanner;

public class MainMenu implements Appearance{

        //PATH SAVE FILE FREKUENSI
        String saveFreq = "src/Challenge2/data_frequensi.txt";
        String saveFreqA = "src/Challenge2/frequensi_kelasA.txt";
        String saveFreqB = "src/Challenge2/frequensi_kelasA.txt";
        String saveFreqC = "src/Challenge2/frequensi_kelasA.txt";
        String saveFreqD = "src/Challenge2/frequensi_kelasA.txt";
        String saveFreqE = "src/Challenge2/frequensi_kelasA.txt";
        String saveFreqF = "src/Challenge2/frequensi_kelasA.txt";
        String saveFreqG = "src/Challenge2/frequensi_kelasA.txt";
        String saveFreqH = "src/Challenge2/frequensi_kelasA.txt";

        //PATH SAVE FILE MEAN MEDIAN MODULUS
        String saveMeanMedMod = "src/Challenge2/data_MeanMedianMod.txt";
        String saveMmmA ="src/Challenge2/3M_kelasA.txt";
        String saveMmmB ="src/Challenge2/3M_kelasB.txt";
        String saveMmmC ="src/Challenge2/3M_kelasC.txt";
        String saveMmmD ="src/Challenge2/3M_kelasD.txt";
        String saveMmmE ="src/Challenge2/3M_kelasE.txt";
        String saveMmmF ="src/Challenge2/3M_kelasF.txt";
        String saveMmmG ="src/Challenge2/3M_kelasG.txt";
        String saveMmmH ="src/Challenge2/3M_kelasH.txt";

        //PATH READ FILE
        String pathSekolah = "src/Challenge2/data_Sekolah.csv";
        String pathA ="src/Challenge2/kelasA.csv";
        String pathB ="src/Challenge2/kelasB.csv";
        String pathC ="src/Challenge2/kelasC.csv";
        String pathD ="src/Challenge2/kelasD.csv";
        String pathE ="src/Challenge2/kelasE.csv";
        String pathF ="src/Challenge2/kelasF.csv";
        String pathG ="src/Challenge2/kelasG.csv";
        String pathH ="src/Challenge2/kelasH.csv";

        ReadWriteKelas rwk = new ReadWriteKelas();
        ReadWriteFile rwf = new ReadWriteFile();

        private final String TITTLE = "Aplikasi Pengolahan Data Nilai Siswa";

        Scanner key = new Scanner(System.in);

        public void Menu() {

            try {
                do {
                    switch (menu()){
                        case "0":{
                            System.out.println();
                            loading();
                            rwk.multiDelete();
                            System.exit(0);
                        }

                        case "1":{
                            loading();
                            rwk.multiPrint("src/Challenge2/data_sekolah.csv");
                            key.nextLine();
                            break;
                        }

                        case "2":{
                            switch (menu1()) {
                                case "0": {
                                    break;
                                }
                                case "1": {
                                    rwf.writeFreqFinder(pathSekolah, saveFreq);
                                    System.out.println();
                                    System.out.println("Success Writting File");
                                    System.out.print("Press enter to back main menu");
                                    key.nextLine();
                                    break;
                                }
                                case "2": {
                                    rwf.writeFreqFinder(pathA, saveFreqA);
                                    System.out.println();
                                    System.out.println("Success Writting File");
                                    System.out.print("Press enter to back main menu");
                                    key.nextLine();
                                    break;
                                }
                                case "3": {
                                    rwf.writeFreqFinder(pathB, saveFreqB);
                                    System.out.println();
                                    System.out.println("Success Writting File");
                                    System.out.print("Press enter to back main menu");
                                    key.nextLine();
                                    break;
                                }
                                case "4": {
                                    rwf.writeFreqFinder(pathC, saveFreqC);
                                    System.out.println();
                                    System.out.println("Success Writting File");
                                    System.out.print("Press enter to back main menu");
                                    key.nextLine();
                                    break;
                                }
                                case "5": {
                                    rwf.writeFreqFinder(pathD, saveFreqD);
                                    System.out.println();
                                    System.out.println("Success Writting File");
                                    System.out.print("Press enter to back main menu");
                                    key.nextLine();
                                    break;
                                }
                                case "6": {
                                    rwf.writeFreqFinder(pathE, saveFreqE);
                                    System.out.println();
                                    System.out.println("Success Writting File");
                                    System.out.print("Press enter to back main menu");
                                    key.nextLine();
                                    break;
                                }
                                case "7": {
                                    rwf.writeFreqFinder(pathF, saveFreqF);
                                    System.out.println();
                                    System.out.println("Success Writting File");
                                    System.out.print("Press enter to back main menu");
                                    key.nextLine();
                                    break;
                                }
                                case "8": {
                                    rwf.writeFreqFinder(pathG, saveFreqG);
                                    System.out.println();
                                    System.out.println("Success Writting File");
                                    System.out.print("Press enter to back main menu");
                                    key.nextLine();
                                    break;
                                }
                                case "9": {
                                    rwf.writeFreqFinder(pathH, saveFreqH);
                                    System.out.println();
                                    System.out.println("Success Writting File");
                                    System.out.print("Press enter to back main menu");
                                    key.nextLine();
                                    break;
                                }
                                default: {
                                    loading();
                                    System.out.println("\nAngka yang anda masukan tidak sesuai atau tidak ada");
                                    System.out.print("Press enter to back main menu");
                                    key.nextLine();
                                    break;
                                }
                            }
                            break;
                        }

                        case "3":{
                            switch (menu2()){
                                case "0":{
                                    break;
                                }
                                case "1":{
                                    rwf.writeMeanMedMod(pathSekolah, saveMeanMedMod);
                                    System.out.println();
                                    System.out.println("Success Writting File");
                                    System.out.print("Press enter to back main menu");
                                    key.nextLine();
                                    break;
                                }
                                case "2":{
                                    rwf.writeMeanMedMod(pathA, saveMmmA);
                                    System.out.println();
                                    System.out.println("Success Writting File");
                                    System.out.print("Press enter to back main menu");
                                    key.nextLine();
                                    break;
                                }
                                case "3":{
                                    rwf.writeMeanMedMod(pathB, saveMmmB);
                                    System.out.println();
                                    System.out.println("Success Writting File");
                                    System.out.print("Press enter to back main menu");
                                    key.nextLine();
                                    break;
                                }
                                case "4":{
                                    rwf.writeMeanMedMod(pathC, saveMmmC);
                                    System.out.println();
                                    System.out.println("Success Writting File");
                                    System.out.print("Press enter to back main menu");
                                    key.nextLine();
                                    break;
                                }
                                case "5":{
                                    rwf.writeMeanMedMod(pathD, saveMmmD);
                                    System.out.println();
                                    System.out.println("Success Writting File");
                                    System.out.print("Press enter to back main menu");
                                    key.nextLine();
                                    break;
                                }
                                case "6":{
                                    rwf.writeMeanMedMod(pathE, saveMmmE);
                                    System.out.println();
                                    System.out.println("Success Writting File");
                                    System.out.print("Press enter to back main menu");
                                    key.nextLine();
                                    break;
                                }
                                case "7":{
                                    rwf.writeMeanMedMod(pathF, saveMmmF);
                                    System.out.println();
                                    System.out.println("Success Writting File");
                                    System.out.print("Press enter to back main menu");
                                    key.nextLine();
                                    break;
                                }
                                case "8":{
                                    rwf.writeMeanMedMod(pathG, saveMmmG);
                                    System.out.println();
                                    System.out.println("Success Writting File");
                                    System.out.print("Press enter to back main menu");
                                    key.nextLine();
                                    break;
                                }
                                case "9":{
                                    rwf.writeMeanMedMod(pathH, saveMmmH);
                                    System.out.println();
                                    System.out.println("Success Writting File");
                                    System.out.print("Press enter to back main menu");
                                    key.nextLine();
                                    break;
                                }
                                default:{
                                    loading();
                                    System.out.println("\nAngka yang anda masukan tidak sesuai atau tidak ada");
                                    System.out.print("Press enter to back main menu");
                                    key.nextLine();
                                    break;
                                }
                            }
                            break;
                        }
                        default:{
                            loading();
                            System.out.println("\nAngka yang anda masukan tidak sesuai atau tidak ada");
                            System.out.print("Press enter to back main menu");
                            key.nextLine();
                            break;
                        }
                    }
                }while (true);
            }catch (Exception e){
                System.out.println("PLEASE GENERATE DATA SEKOLAH");
                System.out.println();
                System.out.println("PRESS ENTER TO BACK MAIN MENU");
                key.nextLine();
                Menu();
            }
        }

    private String menu() {
        header();
        System.out.println("1. Generate Data Sekolah");
        System.out.println("2. Data Frequency");
        System.out.println("3. Data Mean Median Mode");
        System.out.println("0. Exit");
        footer();
        System.out.print("Masukkan Pilihan -> ");
        return key.nextLine();
    }

    private String menu1() {
        header();
        System.out.println("1. Data Sekolah");
        System.out.println("2. Kelas A");
        System.out.println("3. Kelas B");
        System.out.println("4. Kelas C");
        System.out.println("5. Kelas D");
        System.out.println("6. Kelas E");
        System.out.println("7. Kelas F");
        System.out.println("8. Kelas G");
        System.out.println("9. Kelas H");
        System.out.println("0. Kembali ke menu utama");
        footer();
        System.out.print("Masukkan Pilihan -> ");
        return key.nextLine();
    }

    private String menu2() {
        header();
        System.out.println("1. Data Sekolah");
        System.out.println("2. Kelas A");
        System.out.println("3. Kelas B");
        System.out.println("4. Kelas C");
        System.out.println("5. Kelas D");
        System.out.println("6. Kelas E");
        System.out.println("7. Kelas F");
        System.out.println("8. Kelas G");
        System.out.println("9. Kelas H");
        System.out.println("0. Kembali ke menu utama");
        footer();
        System.out.print("Masukkan Pilihan -> ");
        return key.nextLine();
    }


    @Override
    public void separatorTitle(int size) {
        StringBuilder separator = new StringBuilder();
        for (int i = 0; i < size + 6; i++){
            separator.append("-");
        }
        System.out.println(separator);
    }

    @Override
    public void header() {
        String space = "   ";
        separatorTitle(TITTLE.length());
        System.out.println(space+TITTLE);
        separatorTitle(TITTLE.length());
    }

    @Override
    public void footer() {
        int titleLength = TITTLE.length();
        separatorTitle(titleLength);
    }

    @Override
    public void loading() {
        StringBuilder load = new StringBuilder();
        for (int i = 0; i < TITTLE.length(); i++ ){
            load.append("-");
            System.out.println(load);
        }
    }
}

