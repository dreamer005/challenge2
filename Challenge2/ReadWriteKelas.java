package Challenge2;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.PrintStream;

public class ReadWriteKelas implements PrintAction, DeleteAction{

    @Override
    public void multiDelete() {

        File deletefl = new File("src/tempfile");

        String[] s = deletefl.list();

        for (String a : s){
            File f1 = new File(deletefl,a);
            f1.delete();
        }
        System.out.println("SHUTDOWN AND DELETING TEMP FILE SUCCESSFULL... ");
    }

    @Override
    public void multiPrint(String path) {

        try {

            File file = new File(path);
            FileReader fr = new FileReader(file);
            BufferedReader br = new BufferedReader(fr);

            // src save data kelas
            PrintStream a = new PrintStream("src/Challenge2/kelasA.csv");
            PrintStream b = new PrintStream("src/Challenge2/kelasB.csv");
            PrintStream c = new PrintStream("src/Challenge2/kelasC.csv");
            PrintStream d = new PrintStream("src/Challenge2/kelasD.csv");
            PrintStream e = new PrintStream("src/Challenge2/kelasE.csv");
            PrintStream f = new PrintStream("src/Challenge2/kelasF.csv");
            PrintStream g = new PrintStream("src/Challenge2/kelasG.csv");
            PrintStream h = new PrintStream("src/Challenge2/kelasH.csv");

            String line = " ";

            // Read per line
            char kelas = 'A';
            while ((line = br.readLine()) != null) {

                // write and save data per kelas
                if (kelas == 'A'){
                    a.println(line);
                } else if (kelas == 'B'){
                    b.println(line);
                } else if (kelas == 'C'){
                    c.println(line);
                } else if (kelas == 'D'){
                    d.println(line);
                } else if (kelas == 'E'){
                    e.println(line);
                } else if (kelas == 'F'){
                    f.println(line);
                } else if (kelas == 'G'){
                    g.println(line);
                } else if (kelas == 'H') {
                    h.println(line);
                }

                kelas ++;
            }
            br.close();
            System.out.println("GENERATING SUCCESSFULL...");
            System.out.println("PRESS ENTER TO BACK MAIN MENU");
        } catch (Exception e) {
            System.out.println("\n\n\n\n\n\n\n\n\nThere is an error path: " + e.getMessage());
        }
    }
}

