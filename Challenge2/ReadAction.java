package Challenge2;



import java.util.List;

public interface ReadAction {
    public List<Integer> readFile(String path);
}

