package Challenge2;



public interface Appearance {

    public void separatorTitle(int size);
    public void header();
    public void footer();
    public void loading();

}

